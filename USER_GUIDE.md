# User Guide
Last Changed: 2023-07-14, Author: Paul Genz

## Getting Started
0. Download and install [KNIME](https://www.knime.com/downloads) and setup KNIME workspace.
1. Clone the repository and insert the contents (3 KNIME-Workflows + Setup-Filepaths) into your KNIME Workspace.
2. Open the "SETUP_Inventory" Workflow and specify the Sample_Inventory Directory in the Highlighted Component 
5. Now, in the *KNIME Explorer* open each workflow/component and confirm the update of components and save the changes.
6. Now you should be able to work. 

## Working with the Sample Inventory
- for a visual overview of all workflows please check [WORKFLOW_DIGI.pdf](https://git.ufz.de/genz/sample-inventory/-/blob/main/01_Workflow_Documentation/WORKFLOW_DIGI.pdf) or [WORKFLOW_PRINT.pdf](https://git.ufz.de/genz/sample-inventory/-/blob/main/01_Workflow_Documentation/WORKFLOW_PRINT.pdf)

### Adding New Samples
- use workflow: ADD_Samples_to_Inventory
#### Use Case
- new samples arrive at UFZ, they have to be inventorized 
#### Prerequisites 
1. in your local `Nextcloud/Sample_Inventory` open `sample_entry.xlsx`
2. enter all available sample information to `sample_entry.xlsx` (please check `sample_entry_values.xlsx` for documentation on permitted values)
	For this you need to figure out which `sample_nr_project` you will assign to the new samples 
	1. Go into `Sample_Inventory.xlsx` and check the most recent (the highest) integer that a sample has been assigned in `sample_nr_project` (e.g. 065)
	2. In `sample_entry.xlsx` assign the next highest integer (e.g. 066)
	3. Enter all other information      
4. save `sample_entry.xlsx`

#### Necessary Steps to Execute Workflow
1. open KNIME
2. open the ADD_Samples_to_Inventory Workflow
3. Select all nodes (Shortkey: Strg+A)
4. Reset all nodes (Shortkey: F8)
*OPTIONAL: In the Node: "Choose Sample Entry" you can provide a custom path to a file to be used instead of `sample_entry.xlsx` ... make sure it has the exact format* 
Now, make sure, all nodes are still selected, then: 
5. Execute all executable nodes (Shortkey: Shift+F7)
6. DONE! 
 
#### What happens behind the scenes?
1. A **backup-version of the original `Sample_Inventory.xlsx`** is created and saved in `00_Archive/`. The filename will contain the current date & the suffix: "before_new_samples"
2. A **backup-version of the new samples from `sample_entry.xlsx`** is created and saved in `00_Archive/`. The filename will contain the current date.
3. A **new `Sample_Inventory.xlsx`** is created
	- samples listed in `sample_entry.xlsx` will be transferred to `Sample_Inventory.xlsx` and some values will be replaced using the `01_Auxilliary_Files/ID_dictionary.csv`
	- a unique `sample_id` is generated for each sample 
	- columns are always sorted in the same order
	- samples entries are sorted according to `project_abb` and `sample_nr_project`
	- formatting is applied 
 
### Creating Labels
- use workflow: PRINT_Labels
#### Use Case
- new samples arrive at UFZ, they have to be inventorized and also you'd like to print samples for them
- analysis is upcoming and you'd like to print sample labels (e.g. for your LC/Falcon vials)
#### Prerequisites
1. in your local `Nextcloud/Sample_Inventory` open `label_entry.xlsx`
2. enter all available sample information to `label_entry.xlsx` (please check `sample_entry_values.xlsx` for documentation on permitted values)
	NOTE: for the `status_columns` choose whichever information the labels should display
3. save `label_entry.xlsx`

#### Necessary Steps to Execute Workflow
1. open KNIME
2. open the PRINT_Labels Workflow
3. Select all nodes (Shortkey: Strg+A)
4. Reset all nodes (Shortkey: F8)
5. Execute all executable nodes (Shortkey: Shift+F7)
6. DONE! 

#### What happens behind the scenes?
1. The **`01_Auxilliary_Files/Print_Label.xlsx` is created**. It should then be used as a database that LibreWriter can access to create labels.
	- if the samples provided in `Label_Print.xlsx` (more specifically the  `sample_nr_project`) are **already present in `Sample_Inventory.xlsx`** then:
		1. the `sampling_date` will be automatically added to the label of the new sample
		2. the `analysis_date` provided in `Label_Print.xlsx` will be added to the original sample in `Sample_Inventory.xlsx`
2. A **backup-version of the `Print_Label.xlsx`** is saved in `01_Auxilliary_Files/00_Print_Archive`. The filename will contain the current date.
3. Samples, for which **labels are created will be written into `sample_entry_from_labels.xlsx`** --> ADD storage information here before you use it in ADD_to_Sample_Inventory   

### Deleting Samples
- use workflow: REMOVE_Samples
#### Prerequisites
- none
#### Necessary Steps to Execute Workflow
1. open KNIME
2. open the REMOVE_Samples Workflow
3. Select all nodes (Shortkey: Strg+A)
4. Reset all nodes (Shortkey: F8)
5. Select the Node in the Yellow Box labelled: "CHOOSE SAMPLES 
DELETION (PRESS EXECUTE AND OPEN FIRST VIEW)"
6. Select this Node and press SHIFT+F10 (to Execute with Open View
7. Wait until Viewer opens and select samples for deletion. 
8. Press Apply and wait until the window refreshes. Press Close
9. Select all nodes (Shortkey: Strg+A)
10. Execute all executable nodes (Shortkey: Shift+F7)
11. DONE! 

#### What happens behind the scenes?
1. A **backup-version of the original `Sample_Inventory.xlsx`** is created and saved in `00_Archive/`. The filename will contain the current date & the suffix: "before deletion".
1. A **new version of the `Sample_Inventory.xlsx`** is created without the deleted samples
2. The **deleted samples** are saved in the **`01_Auxilliary_Files/Garbage_Inventory.xlsx`**

