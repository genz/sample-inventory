<!-- PROJECT LOGO -->
<br />
<p align="center">
  <h1 align="center">Sample Inventory</h3>
  <p align="center">
    This repo contains the KNIME-workflows needed to maintain the sample inventory.
    <br />
    <a href="https://git.ufz.de/genz/sample-inventory/issues">Report Bug</a>
    ·
    <a href="https://git.ufz.de/genz/sample-inventory/issues">Request Feature</a>
  </p>
</p


<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
* [Getting Started](#getting-started)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [Contact](#contact)


## About the Project

The KNIME-based workflow for management of the [sample inventory](https://git.ufz.de/genz/sample-inventory/) is supposed to streamline the sample management process and make it less prone to errors.   

## Getting Started

- check out the [USER_GUIDE](https://git.ufz.de/genz/sample-inventory/-/blob/main/USER_GUIDE.md)

## Usage

- check out the [USER_GUIDE](https://git.ufz.de/genz/sample-inventory/-/blob/main/USER_GUIDE.md)

## Roadmap

- future plans:
* [x] - in ADD_Samples_to_Inventory Workflow: go to component for choosing `sample_entry.xlsx` and add default-path as a variable based on `setup_filepath.json`  
* [x] create documentation & usage guide
* [x] make subcomponents sync to each other (create components)
* [x] add a dummy sample-inventory to test functionalities 
* [ ] change `setup_filepath.json` to generic filepaths
* [ ] non-manual handling of duplicate files 
- [ ]   create release version

Go to [open issues](https://git.ufz.de/genz/sample-inventory/issues) for a list of proposed features (and known issues).

## Contributing

Concept and Setup of the KNIME workflows: Paul Genz

## Contact

Questions and Ideas to: [paul.genz@ufz.de](paul.genz@ufz.de)

