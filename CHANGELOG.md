### Version: 0.2.4, 2023-07-21
### Fixes:
- fix: when adding samples through sample_entry_from_label.xlsx no duplicate sample_id is introduced

### Version: 0.2.3, 2023-07-20
### Fixes:
- fixed problem in PRINT_Labels workflow: with joining tables sometimes produced a duplicate column "(right)" -->this was due to a faulty input
- also in Joiner 5:82 the sample_type column read from the Sample_Inventory will be dropped and only the sample_type column from the Label_Entry will be kept.

### Version: 0.2.2, 2023-07-14
### Fixes:
- fixed provision of system-specific filepath separator: "\\" on windows and "/" on linux for saving of backup files into archive --> now "\\" is used as a separator ALSO IN LINUX (Knime doesnt care at the current version)

### Version: 0.2.1, 2023-07-14
### Fixes:
- when adding samples get default paths did not provide correct ID_dictionary location due to missing variable

### Version: 0.2, 2023-07-14
### Features:
- added SETUP_Inventory-Workflow: after specifiying Sample_Inventory-Directory, this workflow automatically writes required paths into the setup_filepaths.json 
### Fixes:
- reset all components and workflows before submitting
- changed link-type of all shared nodes to "workflow-relative"
- JSON reader in "Get Default Filepaths"-Shared Component is now taking the ['Path'] Column of the JSON file 

### Version: 0.1.8, 2023-03-21
### Fixes:
- changed get-filepaths-shared node: it now looks for a workflow-relative path (fixed an issue for the developer)
- moved shared nodes into separate folder

### Version: 0.1.7, 2022-11-17
### Fixes:
- removed extra- unwanted row in BIG_Labels Template

### Version: 0.1.6, 2022-11-03
### Fixes:
- changed import format of date_columns to string, so problems with removing of missing values does not make a problem

### Version: 0.1.6, 2022-11-03
### Added Functions:
- for freeze-dried samples a new abbreviation is added to sample_id: DRY, info has to be provided in status_optional
### Fixes:
- if no sample_analysis is entered the rep will be preceeded by a hashtag, if no rep is provided, no hashtag will be added

### Version: 0.1.5, 2022-11-02
### Fixes:
- added option to all Excel-Reader Nodes to import filtered xlsx-files without skipping the filtered rows

### Version: 0.1.4, 2022-11-01
### Added Functions:
- repo now contains the visualization created by Viktoriia Karabtsova in 01_Workflow_Documentation
### Fixes:
- added missing sample_entry column to label_entry.xlsx and integrated in workflow
- fixed label templates to correctly include sample_type & sample_id on label

### Version: 0.1.3, 2022-10-27
### Fixes:
- added missing node descriptions

## Version: 0.1.2, 2022-10-26
### Changes
- in PRINT_Labels:
	- will now recognize "centrifuged" and create Sample ID accordingly
	- add sample_id of new sample, removed sample_id_original in Print_Label.xlsx
	- added formatting to sample_entry_from_labels.xlsx
- in ADD_Samples_to_Inventory
	- will no fail to write backup file if a file with identical name is present: go ahead and change the file name of the present file manually

## Version: 0.1.1, 2022-10-26
### Added Functions
- added folder containing LibreOffice Labels to Repo
- REMOVE_Samples will now output a (almost nicely) formatted garbage_list.xlsx
### Changes
- made filepaths in setup_filepaths.json generic
- added extra filepaths to setup_filepaths.json:
	- filepath_sample_entry_default
	- filepath_label_entry_default
- in ADD_Sample_to_Inventory a default path will now be provided if sample_entry is not directly specified
- in PRINT_Labels:
	- functionality of automatic adding of samples to sample inventory is removed
	- workflow now saves a sample_entry_from_labels.xlsx to make it possible to (manually) add these samples to the sample inventory (storage info has to be added manually)
- renaming of files for better understandability/consistency
	- Print_Label.xlsx to label_entry.xlsx
- renaming of columns for better understandability/consistency
	- label_entry: sample_id_input to sample_id_original_sample
- internal changes to how the workflows are computed
	- created some modular nodes for:
		1) Get Default Filepaths (from setup_filepaths)
		2) Choose Sample Entry
		3) Choose Label Entry
		4) Create Sample ID from Abbreviation Columns
		5) Extract Sample Info to Abbreviation Columns 
		6) Add Formatting to Excel Table (Sample Inventory)
		7) Create Formatted Sample Inventory
		8) Current Date to Row
		9) Get 1st Part Archiving Filepath
		

## Version: 0.1.0, 2022-10-25
### Established functionality
- 3 Workflows established:
	- adding of samples to inventory
	- creation of a table for label printing (and automatic adding of samples to inventory)
	- deletion of samples
- for all workflow backups are created in the archive folde
- everything essential works
